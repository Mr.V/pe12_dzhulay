let firstName = prompt('Введите ваше имя');
let secondName = prompt('Введите вашу фамилию');
let surName = prompt('Введите ваше отчество');

/**
 * @desc Give back workers full name
 * @param {String} a
 * @param {String} b
 * @param {String} c
 * @return {String}
 * **/
function fullName(a, b, c) {
    return a + ' ' + b + ' ' + c;
}

/**
 * @desc Give back workers short name
 * @param {String} a
 * @param {String} b
 * @param {String} c
 * @return {String}
 * **/
function shortName(a, b, c) {
    return a + ' ' + b.charAt(0) + '. ' + c.charAt(0) + '.';
}

alert(fullName(surName, firstName, secondName));
alert(shortName(surName, firstName, secondName));