/**
* @desc Функция которая считает сумму двух чисел
* @param {Number} a
* @param {Number} b
* @return {Number}
**/
function sum(a, b) {
    return a + b;
}

/**
* @desc Умножение двух чисел
* @param {Number} a
* @param {Number} b
* @return {Number}
**/
function multiply(a, b) {
	if (isFinite(a) && isFinite(b)) {
		return a * b;
	}

    return a * b;
}



function diff(a, b) {
	return a - b;
}

/**
* @desc
**/
function hockey(hits, goals) {
	if(isFinite(amountOfHits) || isFinite(goals)) {
		return 0;
	}
	return goals/amountOfHits * 100;
}